package cfsimulator.simulation.agent;

/**
 * Discretization of the InterestVector.
 * Discrete values are loaded using configuration.properties: agent.interestVector.discreteValuesCount
 *
 * Created by janovsky on 3/23/15.
 */
public enum InterestVectorDiscretization {
    CONTINUOUS, DISCRETE
}
