package cfsimulator.simulation.agent;

import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.RandomProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * InterestVector is owned by an Agent. It is a representation of agent's interests / possessions / etc.
 * based on the given problem
 *
 * Created by janovsky on 2/10/15.
 */
public class InterestVector {
    private ArrayList<Double> interestVector;

    /**
     * default constructor, creates a RANDOM InterestVector of the given size
     *
     * @param size size of the interest vector
     */
    public InterestVector(int size, InterestVectorPolarity polarity, InterestVectorDiscretization discretization) {
        interestVector = new ArrayList<>();
        int max = 0, offset = 0, flip = 0;
        switch (polarity) {
            case ANY:
                max = 200;
                offset = 100;
                flip = 1;
                break;
            case POSITIVE:
                max = 100;
                offset = 0;
                flip = 1;
                break;
            case NEGATIVE:
                max = 100;
                offset = 0;
                flip = -1;
                break;
        }
        if (discretization.equals(InterestVectorDiscretization.CONTINUOUS)) {
            for (int i = 0; i < size; i++) {
                interestVector.add(flip * (double) RandomProvider.getInstance().getRandom().nextInt(max) - offset);
            }
        } else if (discretization.equals(InterestVectorDiscretization.DISCRETE)) {
            int count = ConfigurationProvider.getInstance().getIntProperty("agent.interestVector.discreteValuesCount");
            int step = (int) Math.round((double) max / count);
            ArrayList<Double> options = new ArrayList<>();
            for (int i = -offset; i < max; i += step) {
                options.add((double) (flip * i));
            }
            for (int i = 0; i < size; i++) {
                interestVector.add(options.get(RandomProvider.getInstance().getRandom().nextInt(options.size())));
            }
        }
    }

    public InterestVector(ArrayList<Double> interestVector) {
        this.interestVector = interestVector;
    }

    public InterestVector(int size) {
        interestVector = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            interestVector.add(0.0);
        }
    }

    public InterestVector(InterestVector otherInterestVector) {
        interestVector = new ArrayList<>();
        for (int i = 0; i < otherInterestVector.getSize(); i++) {
            interestVector.add(otherInterestVector.get(i));
        }
    }

    public InterestVector(int size, InterestVectorPolarity polarity) {
        this(size, polarity, InterestVectorDiscretization.CONTINUOUS);
    }

    public InterestVector(double constant, int size) {
        interestVector = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            interestVector.add(constant);
        }
    }

    public int getSize() {
        return interestVector.size();
    }

    public String toString() {
        return interestVector.toString();
    }

    public double sum() {
        double sum = 0;
        for (Double d : interestVector) {
            sum += d;
        }
        return sum;
    }

    public double get(int index) {
        return interestVector.get(index);
    }


    public InterestVector subtract(InterestVector vector2) {
        ArrayList<Double> result = new ArrayList<>();
        for (int i = 0; i < interestVector.size(); i++) {
            result.add(interestVector.get(i) - vector2.get(i));
        }
        return new InterestVector(result);
    }

    public InterestVector add(InterestVector vector2) {
        ArrayList<Double> result = new ArrayList<>();
        for (int i = 0; i < interestVector.size(); i++) {
            result.add(interestVector.get(i) + vector2.get(i));
        }
        return new InterestVector(result);
    }

    public InterestVector multiply(int constant) {
        ArrayList<Double> result = new ArrayList<>();
        for (Double anInterestVector : interestVector) {
            result.add(constant * anInterestVector);
        }
        return new InterestVector(result);
    }

    public double manhattanDistance(InterestVector otherVector) {
        double ret = 0;
        for (int i = 0; i < interestVector.size(); i++) {
            ret += Math.abs(interestVector.get(i) - otherVector.get(i));
        }
        return ret;
    }

    public double getAverage() {
        double ret = 0;
        for (Double ivElement : interestVector) {
            ret += ivElement;
        }
        return ret / interestVector.size();
    }

    public double getSumDistanceFrom(double scalar) {
        double ret = 0;
        for (Double ivElement : interestVector) {
            ret += Math.abs(ivElement - scalar);
        }
        return ret;
    }

    public InterestVector sortAscending() {
        ArrayList<Double> list = new ArrayList<>(interestVector);
        Collections.sort(list);
        InterestVector ret = new InterestVector(list);
        return ret;
    }

    public InterestVector sortDescending() {
        ArrayList<Double> list = new ArrayList<>(interestVector);
        Collections.sort(list, (o1, o2) -> ((o1 > o2) ? -1 : 1));
        InterestVector ret = new InterestVector(list);
        return ret;
    }

    public List<Double> getList() {
        return interestVector;
    }
}
