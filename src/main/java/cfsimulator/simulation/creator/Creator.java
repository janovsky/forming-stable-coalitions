package cfsimulator.simulation.creator;


import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import cfsimulator.simulation.simulator.Simulator;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.DatasetProvider;
import cfsimulator.utils.RandomProvider;

import java.util.Collections;
import java.util.List;

/**
 * USE THIS CLASS FOR THE MAIN ENTRY POINT OF THE PROGRAM.
 * Default Creator which initializes all the singletons and the Simulator, then runs the simulator.
 *
 * Created by janovsky on 2/10/15.
 */
public class Creator {
    private static Simulator simulator;

    public static void main(String[] args) {


        init(args);

        try {
            simulator.run(ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfIterations"));
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }

    private static void init(String[] args) {

        //init configuration provider
        ConfigurationProvider.getInstance().init("configuration.properties", args);

        //init logger
        if (!ConfigurationProvider.getInstance().getBooleanProperty("logger.enabled")) {
            List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
            loggers.add(LogManager.getRootLogger());
            for (Logger logger : loggers) {
                logger.setLevel(Level.OFF);
            }
        }

        //init random provider
        RandomProvider.getInstance().init(ConfigurationProvider.getInstance().getIntProperty("simulation.randomSeed"));

        //load energy load profiles
        if ((ConfigurationProvider.getInstance().getBooleanProperty("simulation.evaluation.EnergyPurchasingEvaluationFunction.realData")
                && ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("cfsimulator.simulation.evaluation.EnergyPurchasingEvaluationFunction")) ||
                ((ConfigurationProvider.getInstance().getBooleanProperty("simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction.realData")
                        && ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("cfsimulator.simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction")))) {
            DatasetProvider.getInstance().init();
        }


        //init simulator
        simulator = new Simulator();
        try {
            simulator.init();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

}
