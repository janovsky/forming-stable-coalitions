package cfsimulator.simulation.creator;

import cfsimulator.simulation.simulator.Simulator;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.RandomProvider;

import java.io.File;

/**
 * Currently unused Creator which precomputes the values of all possible coalitions for a single configuration given in args.
 * Can be ran in parallel to create evaluations for all problem instances.
 * Note that the single-threaded approach is faster (see PrecomputedCreatorSingle).
 *
 * Created by janovsky on 3/30/15.
 */
public class PrecomputerCreator {

    private static Simulator simulator;

    public static void main(String[] args) {


        init(args);

    }


    private static void init(String[] args) {

        //init configuration provider
        ConfigurationProvider.getInstance().init("configuration-precomputer", args);


        String ef = ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction");
        ef = ef.substring(ef.lastIndexOf(".") + 1);
        int seed = ConfigurationProvider.getInstance().getIntProperty("simulation.randomSeed");
        int n = ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfAgents");
        File file = new File("dpidpinput/" + ef + "-n" + n + "-s" + seed);
        if (file.exists()) {
            System.exit(0);
        }

        //init random provider
        RandomProvider.getInstance().init(ConfigurationProvider.getInstance().getIntProperty("simulation.randomSeed"));

        //init simulator
        simulator = new Simulator();
        try {
            simulator.init();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
