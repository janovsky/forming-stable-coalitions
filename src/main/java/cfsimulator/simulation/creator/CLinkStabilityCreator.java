package cfsimulator.simulation.creator;


import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionFactory;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.simulation.simulator.Simulator;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.DatasetProvider;
import cfsimulator.utils.RandomProvider;
import cfsimulator.utils.StabilityChecker;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

/**
 * Use this creator for calculating stability of C-Link solutions.
 *
 * Created by janovsky on 4/8/2016.
 */
public class CLinkStabilityCreator {
    private static Simulator simulator;

    public static void main(String[] args) {


        File dir = new File("experiments/C-Link-stability-resources/");
        File[] directoryListing = dir.listFiles();
        StringBuilder results = new StringBuilder();

        if (directoryListing != null) {
            for (File child : directoryListing) {
                // reset providers, counter...
                init(args);
                CoalitionStructure cs = new CoalitionStructure();
                try (BufferedReader br = new BufferedReader(new FileReader(child))) {
                    String line = br.readLine();
                    int n = Integer.parseInt(line);
                    line = br.readLine();
                    while (line != null && !line.isEmpty()) {
                        System.out.println(line);
                        Coalition c = processLine(line);
                        cs.addCoalition(c);
                        line = br.readLine();
                    }

                    StabilityChecker stabilityChecker = new StabilityChecker(cs, simulator.getAgentStorage());
                    double stabilityRatio = stabilityChecker.getStabilityRatio(ConfigurationProvider.getInstance().getIntProperty("simulation.stabilityChecker.maxAlpha"))[0];
                    results.append(n).append(" ").append(stabilityRatio).append("\n");

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            //write results
            String fileName = "experiments/CLinkStabilityResources.txt";
            File file = new File(fileName);

            PrintWriter writer;
            try {
                writer = new PrintWriter(file, "UTF-8");
                writer.print(results);
                writer.close();

            } catch (FileNotFoundException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }


    }

    private static Coalition processLine(String line) {
        StringTokenizer st = new StringTokenizer(line, ",");
        ArrayList<String> agents = new ArrayList<>();
        while (st.hasMoreElements()) {
            agents.add("Agent" + st.nextToken());
        }
        return CoalitionFactory.getInstance().createCoalition(agents, simulator.getAgentStorage());
    }

    private static void init(String[] args) {

        //init configuration provider
        ConfigurationProvider.getInstance().init("configuration.properties", args);

        //init logger
        if (!ConfigurationProvider.getInstance().getBooleanProperty("logger.enabled")) {
            ArrayList<Logger> loggers = Collections.list(LogManager.getCurrentLoggers());
            loggers.add(LogManager.getRootLogger());
            for (Logger logger : loggers) {
                logger.setLevel(Level.OFF);
            }
        }

        //init random provider
        RandomProvider.getInstance().init(ConfigurationProvider.getInstance().getIntProperty("simulation.randomSeed"));

//        RandomProvider.getInstance().getRandom().nextDouble();

        //load energy lod profiles
        if ((ConfigurationProvider.getInstance().getBooleanProperty("simulation.evaluation.EnergyPurchasingEvaluationFunction.realData")
                && ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("simulation.evaluation.EnergyPurchasingEvaluationFunction")) ||
                ((ConfigurationProvider.getInstance().getBooleanProperty("simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction.realData")
                        && ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction")))) {
            DatasetProvider.getInstance().init();
        }


        //init simulator
        simulator = new Simulator();
        try {
            simulator.init();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}

