package cfsimulator.simulation.evaluation;

import cfsimulator.simulation.agent.InterestVector;
import cfsimulator.simulation.coalition.Coalition;

import java.util.List;

/**
 * returns distance to a flat function i.e. to the average
 *
 * Created by janovsky on 3/9/15.
 */
public class DistanceToFlatEvaluationFunction implements EvaluationFunction {
    @Override
    public double evaluateCoalition(Coalition coalition) {
        InterestVector aggregateInterestVector = coalition.getAggregateInterestVector();
        double average = aggregateInterestVector.getAverage();
        double ret = -aggregateInterestVector.getSumDistanceFrom(average);
        return ret;

    }

    @Override
    public void init(List args) {

    }

}
