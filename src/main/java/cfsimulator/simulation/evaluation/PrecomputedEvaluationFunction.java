package cfsimulator.simulation.evaluation;

import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.EvaluationFunctionParser;

import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Used when coalition values are precomputed and then loaded from a file
 *
 * Created by janovsky on 3/27/15.
 */
public class PrecomputedEvaluationFunction implements EvaluationFunction {
    private boolean isInitialized;
    private HashMap<String, Integer> valueMap;


    @Override
    public double evaluateCoalition(Coalition coalition) {
        assert (valueMap != null);
        assert (coalition != null);
        return valueMap.get(coalition.toNumericallyOrderedString());
    }

    @Override
    public void init(List args) {
        String ef = ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction");
        ef = ef.substring(ef.lastIndexOf(".") + 1);
        int seed = ConfigurationProvider.getInstance().getIntProperty("simulation.randomSeed");
        int n = ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfAgents");
        File file = new File("dpidpinput/" + ef + "-n" + n + "-s" + seed);
        if (file.exists()) {
            isInitialized = true;
            valueMap = EvaluationFunctionParser.parseEvaluationFunction(file);
        }

    }


}
