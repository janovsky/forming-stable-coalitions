package cfsimulator.simulation.evaluation;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.agent.InterestVector;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.utils.ConfigurationProvider;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * returns v(C) = sum(min(b+[i], b-[i]))
 * b+ .. vector of surpluses
 * b- .. vector of shortages
 * v(C) represents the amount of resources that can be shared within the coalition
 *
 * Created by janovsky on 2/16/15.
 */
public class MarketBasedEvaluationFunction implements EvaluationFunction {
    @Override
    public double evaluateCoalition(Coalition coalition) {
        int interestVectorSize = ConfigurationProvider.getInstance().getIntProperty("agent.interestVectorSize");
        double unitsExchangedInCoalition = 0;
        Double[] positiveBalance = new Double[interestVectorSize];
        Double[] negativeBalance = new Double[interestVectorSize];
        Arrays.fill(positiveBalance, 0.0);
        Arrays.fill(negativeBalance, 0.0);

        Iterator<Agent> agentsIterator = coalition.getAgentsIterator();
        while (agentsIterator.hasNext()) {
            InterestVector interestVector = agentsIterator.next().getInterestVector();
            for (int i = 0; i < interestVectorSize; i++) {
                double d = interestVector.get(i);
                if (d > 0) {
                    positiveBalance[i] += d;
                } else {
                    negativeBalance[i] += Math.abs(d);
                }
            }
        }

        for (int i = 0; i < interestVectorSize; i++) {
            unitsExchangedInCoalition += Math.min(positiveBalance[i], negativeBalance[i]);
        }
        return unitsExchangedInCoalition;
    }

    @Override
    public void init(List args) {

    }

}
