package cfsimulator.simulation.evaluation;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.RenewableEnergyCoalition;
import cfsimulator.utils.ConfigurationProvider;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This evaluation function assigns values to coalitions based on @code RenewableEnergyEvaluationFunction
 * and also based on number of upvotes and downvotes in the coalition.
 * Resulting value increases with increasing number of upvotes and decreases with increasing number of downvotes.
 *
 * Created by janovsky on 1/30/17.
 */
public class PrioritizedRenewableEnergyEvaluationFunction extends RenewableEnergyEvaluationFunction {

    @Override
    public double evaluateCoalition(Coalition coalition) {
        double renewableValue = super.evaluateCoalition(coalition);
        Iterator<Agent> agentsIterator = coalition.getAgentsIterator();

        double totalCorrelation = 0;

        while (agentsIterator.hasNext()) {
            Agent a = agentsIterator.next();



            if(a.isEnergyStore()) {
                assert (coalition instanceof RenewableEnergyCoalition);
                Map<Integer, Double> committedByStore = ((RenewableEnergyCoalition) coalition).getCommittedByStores().get(a);
                if(committedByStore == null) continue;

                double committed = ((RenewableEnergyCoalition) coalition).getTotalCommittedByStore(a);
                double votes = a.getGridNode().upVotes - a.getGridNode().downVotes;

                totalCorrelation += committed * (double) votes;
            }
        }

        double priorityWeight = ConfigurationProvider.getInstance().getDoubleProperty("renewableScenario.priorityWeight");

        double fitness = priorityWeight * totalCorrelation + renewableValue;
        return fitness;
    }

    @Override
    public void init(List args) {
        // do nothing
    }
}
