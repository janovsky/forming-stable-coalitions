package cfsimulator.simulation.evaluation;

import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.RandomProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * see: An anytime algorithm for optimal coalition structure generation, Rahwan et al, 2009
 *
 * Created by janovsky on 3/27/15.
 */
public class UniformDistributionEvaluationFunction implements EvaluationFunction {

    private HashMap<String, Double> valueMap = new HashMap<>();


    @Override
    public double evaluateCoalition(Coalition coalition) {
        if (ConfigurationProvider.getInstance().getBooleanProperty("randomBasedEvaluationFunctions.storeValues")) {
            String ordered = coalition.toNumericallyOrderedString();
            if (valueMap.get(ordered) != null) return valueMap.get(ordered);
        }
        double ret;
        int coalitionSize = coalition.getSize();
        Random rand = RandomProvider.getInstance().getRandom();
        ret = coalitionSize * rand.nextDouble();
        if (ConfigurationProvider.getInstance().getBooleanProperty("randomBasedEvaluationFunctions.storeValues")) {
            String ordered = coalition.toNumericallyOrderedString();
            valueMap.put(ordered, ret);
        }
        return ret;
    }

    @Override
    public void init(List args) {

    }

}
