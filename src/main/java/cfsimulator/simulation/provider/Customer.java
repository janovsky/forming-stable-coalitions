package cfsimulator.simulation.provider;

/**
 * Customer interface.
 *
 * Created by janovsky on 2/18/15.
 */
public interface Customer {

    void sendOffer(ProviderAgent providerAgent);
}
