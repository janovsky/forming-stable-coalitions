package cfsimulator.simulation.provider;

import org.apache.log4j.Logger;
import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.agent.InterestVector;
import cfsimulator.simulation.agent.InterestVectorPolarity;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.simulation.evaluation.EvaluationFunction;
import cfsimulator.simulation.simulator.AgentStorage;
import cfsimulator.utils.ConfigurationProvider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.PriorityQueue;

/**
 * Unused.
 * A single central agent that deals tasks.
 *
 * Created by janovsky on 2/18/15.
 */
@Deprecated
public class TaskProviderAgent implements ProviderAgent {
    Logger logger = Logger.getLogger(this.getClass());
    private InterestVector task;
    private PriorityQueue<TaskOffer> taskOffers = new PriorityQueue<>();
    private int price;
    private EvaluationFunction evaluationFunction;

    @Override
    public InterestVector showOffers() {
        return task;
    }

    @Override
    public void sell(InterestVector amount, Coalition coalition) {

    }

    @Override
    public void makeAssignments() {

        TaskOffer winner = taskOffers.poll();
        logger.info("making assignments, winner is: " + winner.getCoalition().getState());
        Iterator<Agent> agentsIterator = winner.getCoalition().getAgentsIterator();

        //set winner coalition and assign profit
        winners.add(winner.getCoalition().getName());
        winnersValues.add(winner.getCoalition().getCoalitionValue());

        while (agentsIterator.hasNext()) {
            agentsIterator.next().assignProfit(price);
        }
        taskOffers.clear();
    }

    @Override
    public void acceptOffer(InterestVector offer, Coalition coalition) {
        TaskOffer taskOffer = new TaskOffer(offer, coalition, task);
        taskOffers.add(taskOffer);
    }

    @Override
    public void createOffers() {
        int numberOfCapabilities = ConfigurationProvider.getInstance().getIntProperty("agent.interestVectorSize");
        task = new InterestVector(numberOfCapabilities, InterestVectorPolarity.POSITIVE);
        task = task.multiply(ConfigurationProvider.getInstance().getIntProperty("provider.taskProvider.difficulty"));
        price = ConfigurationProvider.getInstance().getIntProperty("provider.taskProvider.price");
        logger.info("creating offers: " + task.toString() + ", price: " + price);
    }

    @Override
    public ArrayList<String> getWinners() {
        return winners;
    }

    @Override
    public ArrayList<Double> getWinnersValues() {
        return winnersValues;
    }

    @Override
    public void evaluateAllCombinations() {
        //TODO
    }

    @Override
    public EvaluationFunction getEvaluationFunction() {
        return evaluationFunction;
    }

    @Override
    public double evaluateSingle(Coalition coalition) {
        return evaluationFunction.evaluateCoalition(coalition);
    }

    @Override
    public void evaluateAll(CoalitionStructure coalitionStructure) {

    }

    @Override
    public void init(EvaluationFunction evaluationFunction, AgentStorage agentStorage, int id) {
        this.evaluationFunction = evaluationFunction;
    }
}
