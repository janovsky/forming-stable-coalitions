package cfsimulator.simulation.provider;

import cfsimulator.simulation.agent.InterestVector;
import cfsimulator.simulation.coalition.Coalition;

/**
 * Unused.
 *
 * Created by janovsky on 2/18/15.
 */
@Deprecated
public class TaskOffer implements Comparable {
    private final InterestVector offer;
    private final Coalition coalition;
    private double value;

    public TaskOffer(InterestVector offer, Coalition coalition, InterestVector assignedTask) {
        this.offer = offer;
        this.coalition = coalition;
        evaluate(assignedTask);
    }

    private void evaluate(InterestVector assignedTask) {
        this.value = offer.subtract(assignedTask).sum();
    }

    @Override
    public int compareTo(Object o) {
        TaskOffer other = (TaskOffer) o;
        if (value > other.getValue()) {
            return -1;
        } else {
            return 1;
        }
    }

    public double getValue() {
        return value;
    }

    public Coalition getCoalition() {
        return coalition;
    }
}
