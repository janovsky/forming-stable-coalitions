package cfsimulator.simulation.provider;

import cfsimulator.simulation.agent.InterestVector;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.simulation.evaluation.EvaluationFunction;
import cfsimulator.simulation.simulator.AgentStorage;
import cfsimulator.utils.ConfigurationProvider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.PriorityQueue;

/**
 * Currently unused.
 * A central agent that operates with offers and goods.
 *
 * Created by janovsky on 2/18/15.
 */
@Deprecated
public class GroupDiscountGoodsProviderAgent implements ProviderAgent {
    private InterestVector maxCoalitionSizes;
    private EvaluationFunction evaluationFunction;
    private ArrayList<Double> winnersValues;

    /**
     * the interest vector shows the maximum coalition size for each good
     *
     * @return
     */
    @Override
    public InterestVector showOffers() {
        return maxCoalitionSizes;
    }

    /**
     * the goods are not actually sold, only the price is calculated using the evaluation methods
     *
     * @param amount
     * @param coalition
     */
    @Override
    public void sell(InterestVector amount, Coalition coalition) {

    }

    /**
     * the goods are not actually assigned, only the price is calculated using the evaluation methods
     */
    @Override
    public void makeAssignments() {

    }

    @Override
    public void acceptOffer(InterestVector offer, Coalition coalition) {

    }

    @Override
    public void createOffers() {
        ArrayList<Double> list = new ArrayList<>();
        for (int i = 0; i < ConfigurationProvider.getInstance().getIntProperty("agent.interestVectorSize"); i++) {
            list.add((double) ConfigurationProvider.getInstance().getIntProperty("resourceProviderAgent.maxCoalitionSize"));
        }
        this.maxCoalitionSizes = new InterestVector(list);
    }

    @Override
    public ArrayList<String> getWinners() {
        return winners;
    }

    @Override
    public ArrayList<Double> getWinnersValues() {
        return null;
    }

    @Override
    public void evaluateAllCombinations() {
        //TODO
    }

    @Override
    public EvaluationFunction getEvaluationFunction() {
        return evaluationFunction;
    }

    @Override
    public double evaluateSingle(Coalition coalition) {
        return evaluationFunction.evaluateCoalition(coalition);
    }

    /**
     * find a winner
     * @param coalitionStructure
     */
    @Override
    public void evaluateAll(CoalitionStructure coalitionStructure) {
        PriorityQueue<Coalition> queue = new PriorityQueue<>();
        Iterator<Coalition> openCoalitionIterator = coalitionStructure.getOpenCoalitionIterator();
        while (openCoalitionIterator.hasNext()) {
            queue.add(openCoalitionIterator.next());
        }
        Coalition winner = queue.poll();
        winners.add(winner.getName());
        winnersValues.add(winner.getCoalitionValue());
    }

    @Override
    public void init(EvaluationFunction evaluationFunction, AgentStorage agentStorage, int id) {
        this.evaluationFunction = evaluationFunction;
    }
}
