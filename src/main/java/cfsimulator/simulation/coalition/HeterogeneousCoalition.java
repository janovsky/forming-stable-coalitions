package cfsimulator.simulation.coalition;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.evaluation.EvaluatorAgent;
import cfsimulator.simulation.simulator.AgentStorage;
import cfsimulator.utils.CollectionsProvider;

import java.util.ArrayList;

/**
 * This coalition can have multiple values based on multiple valuation functions.
 *
 * Created by janovsky on 4/29/16.
 */
public class HeterogeneousCoalition extends Coalition {
    private final ArrayList<EvaluatorAgent> evaluatorAgentList;
    private ArrayList<Double> valueList;

    protected HeterogeneousCoalition(String name, Agent a, AgentStorage agentStorage, ArrayList<EvaluatorAgent> evaluatorAgentList) {

        super(name, a, agentStorage);
        this.evaluatorAgentList = evaluatorAgentList;
        valueList = CollectionsProvider.createDoubleList(evaluatorAgentList.size(), 0.0);
        updateValue();
    }

    protected HeterogeneousCoalition(String name, ArrayList<String> agentNames, AgentStorage agentStorage, ArrayList<EvaluatorAgent> evaluatorAgentList) {
        super(name, agentNames, agentStorage);
        this.evaluatorAgentList = evaluatorAgentList;
        valueList = CollectionsProvider.createDoubleList(this.evaluatorAgentList.size(), 0.0);
        updateValue();
    }

    @Override
    protected void updateValue() {
        //TODO only supports entry marginal contribution
        if(evaluatorAgentList == null) return;
        for (int id = 0; id < evaluatorAgentList.size(); id++) {
            valueList.set(id, evaluatorAgentList.get(id).evaluateSingle(this));
        }
    }


    @Override
    public double getEntryMarginalContribution(String agentName) {
        //TODO: For now we have a single list of marginal contributions, one value for each agent depending on the agent's utilityTypeID
        // This might change later, we could save multiple values for each agent, one per each evaluator agent
        return super.getEntryMarginalContribution(agentName);
    }


    /**
     * update entry marginal contributions for all agents given the utility type of each agent
     */
    @Override
    protected void updateEntryMarginalContributions() {
        double[] values = new double[evaluatorAgentList.size()];
        ArrayList<String> temp = new ArrayList<>();
        for (String agentName : agentsOrder) {
            temp.add(agentName);
            Agent agent = getAgentsMap().get(agentName);
            int utilityTypeID = agent.getUtilityTypeID();
            HeterogeneousCoalition c = (HeterogeneousCoalition) CoalitionFactory.getInstance().createCoalition(temp, agentStorage);
            marginalContributions.put(agentName, c.getCoalitionValue(agent) - values[utilityTypeID]);
            for (int i = 0; i < values.length; i++) {
                values[i] = c.getCoalitionValue(agent);
            }
        }
    }

    @Override
    public Double getCoalitionValue() {
        //TODO think this through, what should be returned?
        return super.getCoalitionValue();
    }

    /**
     * @param askingAgent agent that is asking for the coalition value
     * @return coalition value from the point of view of the askingAgent
     */
    @Override
    public Double getCoalitionValue(Agent askingAgent) {
        int evaluatorAgentID = askingAgent.getUtilityTypeID();
        return valueList.get(evaluatorAgentID);
    }

    @Override
    public double getCurrentMarginalContribution(String agentName) {
        throw new UnsupportedOperationException("Heterogeneous coalition only supports entry marginal contribution");
    }

    @Override
    public double getEqualSharingProfit(String agentName) {
        throw new UnsupportedOperationException("Heterogeneous coalition only supports entry marginal contribution");
    }

    @Override
    public double getFairProfit(String agentName) {
        throw new UnsupportedOperationException("Heterogeneous coalition only supports entry marginal contribution");
    }
}
