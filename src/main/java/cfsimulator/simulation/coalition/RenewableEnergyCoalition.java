package cfsimulator.simulation.coalition;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.agent.InterestVector;
import cfsimulator.simulation.simulator.AgentStorage;
import cfsimulator.utils.ConfigurationProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Coalition containing a single renewable generator and possibly multiple energy stores
 *
 * Created by janovsky on 7/20/16.
 */
public class RenewableEnergyCoalition extends Coalition {

    private InterestVector requiredEnergy;
    private List<Double> committedEnergy;



    private Map<Agent, Map<Integer, Double>> committedByStores;
    private Map<Agent, Map<Integer, Double>> committedUsedByStores;
    private List<Double> committedUsedEnergy;

    public RenewableEnergyCoalition(String name, Agent a, AgentStorage agentStorage) {
        super(name, a, agentStorage);
        init(a);
    }

    public RenewableEnergyCoalition(String name, ArrayList<String> agentNames, AgentStorage agentStorage) {
        super(name, agentNames, agentStorage);
    }


    @Override
    public void init(Agent a)
    {
        committedEnergy = new ArrayList<>();
        committedByStores = new HashMap<>();
        committedUsedByStores = new HashMap<>();

        int numberOfSlots = ConfigurationProvider.getInstance().getIntProperty("renewableScenario.numberOfSlots");
        //initialize for each time slot
        for (int i = 0; i < numberOfSlots; i++) {
            committedEnergy.add(0.0);
        }

        requiredEnergy = a.getInterestVector();
        leader = a;
    }

    @Override
    public void addAgent(Agent agent, boolean updateValue) {
        super.addAgent(agent, updateValue);
        committedByStores.put(agent, new HashMap<>());
        InterestVector agentData = agent.getInterestVector();
        double totalAmount = agentData.get(0);
        double remainingAmount = totalAmount;
        int availableFrom = (int) agentData.get(1);
        int availableTo = (int) agentData.get(2);


        remainingAmount = splitEvenly(agent, remainingAmount, availableFrom, availableTo);
//        remainingAmount = splitByPriority(agent, remainingAmount, availableFrom, availableTo);


        updateValue();

        double committedAmount = totalAmount - remainingAmount;
        marginalContributions.put(agent.getName(), committedAmount);
    }

    private double splitByPriority(Agent agent, double remainingAmount, int availableFrom, int availableTo) {
        double[] upVotesVector = agent.getGridNode().upVotesVector;
        double[] downVotesVector = agent.getGridNode().downVotesVector;

        ArrayList<Integer> order = new ArrayList<>();
        for (int i = availableFrom; i < availableTo; i++) {
            order.add(i);
        }

        order.sort((i1, i2) -> - (int)Math.signum((upVotesVector[i1] - downVotesVector[i1]) + (upVotesVector[i2] - downVotesVector[i2])));


            // try to assign as much as possible given the order
            for(int i : order)
            {
                double available = requiredEnergy.get(i) - committedEnergy.get(i);
                if(available > 0)
                {
                    // add as much as we can
                    remainingAmount = addToSlot(agent, remainingAmount, remainingAmount, i, available);
                    assert(remainingAmount >= 0);
                }
            }

            return remainingAmount;
    }

    /**
     * split the total amount evenly
     * @param agent
     * @param remainingAmount
     * @param availableFrom
     * @param availableTo
     * @return
     */
    private double splitEvenly(Agent agent, double remainingAmount, int availableFrom, int availableTo) {

        boolean added = true;
        double minSplit = 0.1;
        while(added)
        {
            added = false;
            int numAvailable = findAvailable(requiredEnergy, committedEnergy, availableFrom, availableTo);
            double split = remainingAmount / numAvailable;
            if(split < minSplit) break;
            for (int i = availableFrom; i <= availableTo; i++) {
                double available = requiredEnergy.get(i) - committedEnergy.get(i);
                if(available > 0)
                {
                    remainingAmount = addToSlot(agent, remainingAmount, split, i, available);
                    added = true;
                }
            }
        }
        return remainingAmount;
    }

    private double addToSlot(Agent agent, double remainingAmount, double amountWantsToGive, int slot, double available) {
        double add = Math.min(amountWantsToGive, available);
        committedEnergy.set(slot, committedEnergy.get(slot) + add);

        // record how much the store committed to for this slot
        committedByStores.get(agent).putIfAbsent(slot, 0.0);
        committedByStores.get(agent).compute(slot, (k,v) -> v + add);

        remainingAmount -= add;
        return remainingAmount;
    }

    @Override
    public void removeAgent(String agentName, boolean update) {
        for (int i = 0; i < committedEnergy.size(); i++) {
            double added = committedByStores.get(getAgentsMap().get(agentName)).getOrDefault(i, 0.0);
            committedEnergy.set(i, committedEnergy.get(i) - added);
        }
        committedByStores.remove(getAgentsMap().get(agentName));
        super.removeAgent(agentName, update);
    }

    private int findAvailable(InterestVector requiredEnergy, List<Double> committedEnergy, int availableFrom, int availableTo) {
        int numAvailable = 0;
        for (int i = availableFrom; i <= availableTo; i++) {
            if(requiredEnergy.get(i) > committedEnergy.get(i))
                numAvailable ++;
        }
        return numAvailable;
    }

    public List<Double> getCommittedEnergy() {
        return committedEnergy;
    }

    public InterestVector getRequiredEnergy() {
        return requiredEnergy;
    }

    public void setCommittedUsedEnergy(List<Double> committedUsedEnergy) {
        this.committedUsedEnergy = committedUsedEnergy;
    }

    public List<Double> getCommittedUsedEnergy() {
        return committedUsedEnergy;
    }

    public Map<Agent, Map<Integer, Double>> getCommittedByStores() {
        return committedByStores;
    }

    public double getPercentageCommittedByStores() {
        int numberOfSlots = ConfigurationProvider.getInstance().getIntProperty("renewableScenario.numberOfSlots");
        double percentage = 0;
        for (int i = 0; i < numberOfSlots; i++) {
            if(requiredEnergy.get(i) != 0)
                percentage += committedEnergy.get(i) / requiredEnergy.get(i);
        }
        percentage /= numberOfSlots;
        assert(percentage >= 0 && percentage <= 1);

        return percentage;
    }

    public double getTotalCommittedByStore(Agent agent) {
        Map<Integer, Double> commitmentMap = committedByStores.get(agent);

        double total = 0;
        for(Double d : commitmentMap.values())
        {
            total += d;
        }
        return total;
    }

    public double getTotalUsedByStore(Agent agent) {
        Map<Integer, Double> commitmentMap = committedUsedByStores.get(agent);

        double total = 0;
        for(Double d : commitmentMap.values())
        {
            total += d;
        }
        return total;
    }

    public void setCommittedUsedByStoreEnergy(Agent a, int slot, double amountUsed) {
        Map<Integer, Double> map = committedUsedByStores.computeIfAbsent(a, agent->new HashMap<>());
        map.put(slot, amountUsed);
    }


}
