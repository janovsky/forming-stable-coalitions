package cfsimulator.simulation.coalition;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.provider.ProviderAgent;
import cfsimulator.simulation.simulator.AgentStorage;

import java.util.*;

/**
 * A structure that holds all coalitions. Every agent participates in exactly one coalition.
 *
 * Created by janovsky on 2/13/15.
 */
public class CoalitionStructure implements Comparable {
    private HashMap<String, Coalition> openCoalitionMap = new HashMap<>();
    private HashMap<String, Coalition> closedCoalitionMap = new HashMap<>();
    private Double value = null;


    private Double maxCoalitionValue = null;
    private boolean stabilityComputationComplete = false;
    private ArrayList<Double> alphaStableList;
    private double fitness;

    /**
     * This constructor is to be used to reconstruct a CS based on a representative string
     *
     * @param coalitionStructureString example: {1,2}{5}{3,4}
     * @param agentStorage agent storage
     */
    public CoalitionStructure(String coalitionStructureString, AgentStorage agentStorage) {
        StringTokenizer st = new StringTokenizer(coalitionStructureString, "{");
        while (st.hasMoreTokens()) {
            String coalitionString = st.nextToken();
            coalitionString = coalitionString.substring(0, coalitionString.length() - 1);
            StringTokenizer coalitionST = new StringTokenizer(coalitionString, ",");
            ArrayList<String> agentNames = new ArrayList<>();
            while (coalitionST.hasMoreTokens()) {
                agentNames.add(coalitionST.nextToken());
            }
            Coalition c = CoalitionFactory.getInstance().createCoalition(agentNames, agentStorage);
            openCoalitionMap.put(c.getName(), c);
        }
        evaluate();
    }

    public CoalitionStructure() {
    }

    public void addCoalition(Coalition coalition) {
        coalition.setNew(false);
        openCoalitionMap.put(coalition.getName(), coalition);
        evaluate();
    }

    public HashMap<String, Coalition> getOpenCoalitionMap() {
        return openCoalitionMap;
    }

    public void closeCoalition(String name) {
        closedCoalitionMap.put(name, openCoalitionMap.get(name));
        openCoalitionMap.remove(name);
        evaluate();
    }

    public void openCoalition(String name) {
        openCoalitionMap.put(name, closedCoalitionMap.get(name));
        closedCoalitionMap.remove(name);
        evaluate();
    }

    public void removeCoalition(String name) {
        openCoalitionMap.remove(name);
        closedCoalitionMap.remove(name);
        evaluate();
    }

    public Iterator<Coalition> getOpenCoalitionIterator() {
        return openCoalitionMap.values().iterator();
    }

    public int getNumberOfOpenCoalitions() {
        return openCoalitionMap.size();
    }

    public void sendOffers(ProviderAgent providerAgent) {
        openCoalitionMap.forEach((k, v) -> {
            if (v != null) v.sendOffer(providerAgent);
        });
    }

    public void evaluate() {
        Iterator<Coalition> openCoalitionIterator = getOpenCoalitionIterator();
        double value = 0;
        double maxCoalitionValue = -Double.MAX_VALUE;
        while (openCoalitionIterator.hasNext()) {
            Coalition c = openCoalitionIterator.next();
            value += c.getCoalitionValue();
            if (c.getCoalitionValue() > maxCoalitionValue) {
                maxCoalitionValue = c.getCoalitionValue();
            }
        }
        this.value = value;
        this.maxCoalitionValue = maxCoalitionValue;
    }

    public double getValue() {
        if (value == null) {
            evaluate();
        }
        return value;
    }

    public Double getMaxCoalitionValue() {
        if (maxCoalitionValue == null) {
            evaluate();
        }
        return maxCoalitionValue;
    }

    @Override
    public int compareTo(Object o) {
        CoalitionStructure other = (CoalitionStructure) o;
        if (value > other.getValue()) {
            return -1;
        } else if (value < other.getValue()) {
            return 1;
        } else {
            if (maxCoalitionValue > other.getMaxCoalitionValue()) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    /**
     * coalitions ordered lexicographically within the CS, order of agents is preserved based on their entry order
     *
     * @return coalitions ordered lexicographically within the CS
     */
    public String toOrderedString() {
        PriorityQueue<String> q = new PriorityQueue<>();
        StringBuilder sb = new StringBuilder();
        Iterator<Coalition> openCoalitionIterator = getOpenCoalitionIterator();
        while (openCoalitionIterator.hasNext()) {
            q.add(openCoalitionIterator.next().toEntryOrderedString());
        }
        while (!q.isEmpty()) {
            sb.append(q.poll());
        }
        return sb.toString();
    }

    public boolean isStabilityComputationComplete() {
        return stabilityComputationComplete;
    }

    public void setStabilityComputationComplete(boolean stabilityComputationComplete) {
        this.stabilityComputationComplete = stabilityComputationComplete;
    }

    public ArrayList<Double> getAlphaStableList() {
        return alphaStableList;
    }

    public void setAlphaStableList(ArrayList<Double> alphaStableList) {
        this.alphaStableList = alphaStableList;
    }

    public double getAlphaStability(int alphaTop) {
        if (alphaStableList.isEmpty()) return 0;
        if (alphaStableList.size() <= alphaTop) return alphaStableList.get(alphaStableList.size() - 1);
        return alphaStableList.get(alphaTop);
    }

    public double getAlphaStability() {
        if (alphaStableList.isEmpty()) return -1.0;
        return alphaStableList.get(alphaStableList.size() - 1);
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public Coalition getCoalitionFor(Agent agent) {
        Iterator<Coalition> openCoalitionIterator = getOpenCoalitionIterator();
        while (openCoalitionIterator.hasNext()) {
            Coalition c = openCoalitionIterator.next();
            if (c.contains(agent.getName())) {
                return c;
            }
        }
        return null;
    }
}
