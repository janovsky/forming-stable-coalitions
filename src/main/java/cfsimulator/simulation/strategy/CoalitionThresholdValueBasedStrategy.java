package cfsimulator.simulation.strategy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionFactory;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.simulation.coalition.PotentialCoalition;
import cfsimulator.utils.ConfigurationProvider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Unused.
 *
 * Created by janovsky on 2/16/15.
 */
@Deprecated
public class CoalitionThresholdValueBasedStrategy implements Strategy {

    /**
     * leave if coalition was not among first n in last round
     *
     * @param coalition
     * @param agent
     * @return
     */
    @Override
    public boolean leaveCoalition(Coalition coalition, CoalitionStructure cs, Agent agent) {
        ArrayList<String> winners = coalition.getEvaluatorAgent().getWinners();
        int f = (int) Math.round((double) ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfAgents")
                / ConfigurationProvider.getInstance().getIntProperty("CoalitionValueBasedStrategy.firstN"));
        List<String> firstN = winners.subList(0, Math.min(f, winners.size()));
        return !firstN.contains(coalition.getName());
    }

    /**
     * pick a coalition that will benefit the most from the addition of this agent
     *
     * @param coalitionStructure
     * @param agent
     * @return
     */
    @Override
    public Coalition pickCoalition(CoalitionStructure coalitionStructure, Agent agent) {
        Iterator<Coalition> openCoalitionIterator = coalitionStructure.getOpenCoalitionIterator();
        PriorityQueue<PotentialCoalition> queue = new PriorityQueue<>();
        while (openCoalitionIterator.hasNext()) {
            Coalition c = openCoalitionIterator.next();
            PotentialCoalition p = new PotentialCoalition(c, agent);
            queue.add(p);
        }
        PotentialCoalition pBest = queue.poll();
        Coalition alone = CoalitionFactory.getInstance().createCoalition(agent, agent.getAgentStorage());
        if (pBest == null || alone.getCoalitionValue(agent) > pBest.getPotentialValue()) {
            return alone;
        } else {
            return pBest.getCoalition();
        }
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
