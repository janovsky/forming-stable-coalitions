package cfsimulator.simulation.strategy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.utils.RandomProvider;

import java.util.ArrayList;

/**
 * Strategy that advices the agent to stay in a coalition only if the coalition won in last two iterations.
 * Unused.
 *
 * Created by janovsky on 2/20/15.
 */
@Deprecated
public class StayIfWinTwiceStrategy extends StayIfWinStrategy {

    /**
     * leave if the coalition didn't win in both two last iterations.
     *
     * @param coalition
     * @param cs
     * @param agent
     * @return
     */
    @Override
    public boolean leaveCoalition(Coalition coalition, CoalitionStructure cs, Agent agent) {
        ArrayList<String> winners = agent.getAgentStorage().getEvaluatorAgent().getWinners();
        if (winners.isEmpty()) {
            return RandomProvider.getInstance().getRandom().nextBoolean();
        }
        if (winners.size() == 1) {
            return !(winners.get(0).equals(coalition.getName()));
        }
        return !(winners.get(winners.size() - 1).equals(coalition.getName()) && winners.get(winners.size() - 2).equals(coalition.getName()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
