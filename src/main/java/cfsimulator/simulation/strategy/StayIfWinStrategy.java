package cfsimulator.simulation.strategy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionFactory;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.utils.RandomProvider;

import java.util.ArrayList;

/**
 * Strategy that advices the agent to stay in a coalition only if the coalition won in last iteration.
 *
 * Created by janovsky on 2/18/15.
 */
public class StayIfWinStrategy implements Strategy {
    /**
     * leave if the coalition wasn't the best in the last iteration
     *
     * @param coalition
     * @param cs
     * @param agent
     * @return
     */
    @Override
    public boolean leaveCoalition(Coalition coalition, CoalitionStructure cs, Agent agent) {
        if(coalition == null) return true;

        ArrayList<String> winners = agent.getAgentStorage().getEvaluatorAgent().getWinners();
        if (winners.isEmpty()) {
            return RandomProvider.getInstance().getRandom().nextBoolean();
        }
        return !(winners.get(winners.size() - 1).equals(coalition.getName()));
    }

    /**
     * pick a strategy randomly
     * @param coalitionStructure
     * @param agent
     * @return
     */
    @Override
    public Coalition pickCoalition(CoalitionStructure coalitionStructure, Agent agent) {
        //TODO: IMPLEMENT, THIS IS JUST A RANDOM STRATEGY COPY
        double stayAlone = RandomProvider.getInstance().getRandom().nextDouble();
        //create a new coalition for yourself only
        if (stayAlone < 1.0 / (coalitionStructure.getOpenCoalitionMap().size() + 1)) {
            Coalition newCoalition = CoalitionFactory.getInstance().createCoalition(agent, agent.getAgentStorage());
            return newCoalition;
        }
        Object[] coalitions = coalitionStructure.getOpenCoalitionMap().values().toArray();
        return (Coalition) coalitions[RandomProvider.getInstance().getRandom().nextInt(coalitions.length)];
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
