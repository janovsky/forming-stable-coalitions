package cfsimulator.renewableEnergy;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.linear.FieldMatrix;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import cfsimulator.simulation.agent.Agent;
import cfsimulator.utils.ConfigurationProvider;

import java.util.List;
import java.util.Map;

/**
 * This class provides an implementation of the load-flow algorithm, which is used widely
 * in power systems community to calculate voltage changes based on power changes.
 *
 * Created by janovsky on 1/20/17.
 */
public class LoadFlow {

    private static final int numberOfSlots = ConfigurationProvider.getInstance().getIntProperty("renewableScenario.numberOfSlots");
    private static final double basePower = 10000000;
    private static final Complex ZERO = new Complex(0, 0);
    private static int n;
    private static double[][] impR;
    private static double[][] impI;

    public static void loadFlowDay(Map<Agent, List<Double>> generation, double[][] impR, double[][] impI, Result result) {
        LoadFlow.impR = impR;
        LoadFlow.impI = impI;

        n = generation.size();

        // in case this result was already evaluated, we need to reset the counters
        result.numberOfNegViolations = 0;
        result.numberOfPosViolations = 0;


        for (int i = 0; i < numberOfSlots; i++) {
            loadFlow(generation, i, result);
        }

    }

    private static void loadFlow(Map<Agent, List<Double>> generation, int slot, Result result) {
        double[] pData = new double[n];
        double[] qData = new double[n];

        // create vector of real loads for the given slot
        generation.forEach((a, g) ->
        {
            int id = a.getId();
            // multiply by -1 because the algorithm (unlike us) assumes positive load and negative generation
            pData[id - 1] = -g.get(slot) / basePower;


            if(ConfigurationProvider.getInstance().getBooleanProperty("renewableScenario.includeQ"))
            {
                // calculate Q for loads
                qData[id - 1] = a.participatesInCF() ? 0.0 : getQ(pData[id - 1]);
            }
            else
            {
                qData[id - 1] = 0;
            }
        });
        RealVector p = MatrixUtils.createRealVector(pData);
        FieldMatrix<Complex> v1 = createComplexMatrix(new Complex(1, 0), n - 1, 1);
        FieldMatrix<Complex> v = createComplexMatrix(new Complex(1, 0), n - 1, 1);

        // impedance matrices. These are triangular matrices.
        RealMatrix R = MatrixUtils.createRealMatrix(impR);
        RealMatrix X = MatrixUtils.createRealMatrix(impI);

        Complex[][] zlineData = new Complex[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                zlineData[i][j] = new Complex(R.getEntry(i, j), X.getEntry(i, j));
            }
        }
        FieldMatrix<Complex> zline = MatrixUtils.createFieldMatrix(zlineData);

        // skip some matlab code

        RealMatrix M_relation = MatrixUtils.createRealMatrix(n, n);
        // n - 1 represents setting last row to a vector of zeros
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    if (!zline.getEntry(i, j).equals(ZERO)) {
                        M_relation.setEntry(i, j, 1);
                    }
                }
            }
        }

        // BIBC matrix
        // -1 represents indexing from 0 (Matlab indexes from 1)
        int a = -1;
        RealMatrix A = MatrixUtils.createRealMatrix(n, n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    if (M_relation.getEntry(i, j) == 1) {
                        a++;
                        if (i == 0) {
                            A.setEntry(a, j, 1);
                        } else {
                            for (int k = 0; k < n; k++) {
                                A.setEntry(k, j, A.getEntry(k, i));
                            }
                            A.setEntry(a, j, 1);
                        }
                    }
                }
            }
        }

        // remove last row and first column and store A in BIBC
//        RealMatrix BIBC = MatrixUtils.createRealMatrix(n - 1, n - 1);
        Complex[][] BIBCdata = new Complex[n - 1][n - 1];
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                //BIBC.setEntry(i, j, A.getEntry(i, j + 1));
                BIBCdata[i][j] = new Complex(A.getEntry(i, j + 1), 0);
            }
        }
        FieldMatrix<Complex> BIBC = MatrixUtils.createFieldMatrix(BIBCdata);


        // BCBV matrix
        // -1 represents indexing from 0 (Matlab indexes from 1)
        int b = -1;
        Complex[][] Bdata = new Complex[n][n];
        // initialize Bdata with zeros
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                Bdata[i][j] = new Complex(0, 0);
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    if (M_relation.getEntry(i, j) == 1) {
                        b++;
                        if (i == 0) {
                            Bdata[j][b] = zline.getEntry(i, j);
                        } else {
                            for (int k = 0; k < n; k++) {
                                Bdata[j][k] = Bdata[i][k];
                            }
                            Bdata[j][b] = zline.getEntry(i, j);
                        }
                    }
                }
            }
        }

        // remove first row and last column and store B in BCBV
        Complex[][] BCBVdata = new Complex[n - 1][n - 1];
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                BCBVdata[i][j] = Bdata[i + 1][j];
            }
        }
        FieldMatrix<Complex> BCBV = MatrixUtils.createFieldMatrix(BCBVdata);
        FieldMatrix<Complex> DLF = BCBV.multiply(BIBC);

        // start loop

        // -1 represents indexing from 0 (Matlab indexes from 1)
        int rep = 1;

        // firstV and secondV represent extending the size of matrix v and using only last 2 columns
        // firstV ~ v(:, sd)
        // secondV ~ v(:, sd + 1)

        FieldMatrix<Complex> firstV = v;
        FieldMatrix<Complex> secondV;

        int iter = 0;
        int maxIter = ConfigurationProvider.getInstance().getIntProperty("renewableScenario.loadFlow.maxIterations");
        do {
            Complex[][] I_loadData = new Complex[n - 1][1];
            for (int k = 0; k < n - 1; k++) {

                // TODO INCLUDE IMAGINARY PART OF INPUT HERE!!!
                Complex c = new Complex(p.getEntry(k), qData[k]);
                c = c.divide(firstV.getEntry(k, 0));
                I_loadData[k][0] = c.conjugate();
            }
            FieldMatrix<Complex> I_load = MatrixUtils.createFieldMatrix(I_loadData);
            // skip some Matlab code

            FieldMatrix<Complex> delta_v = DLF.multiply(I_load);
            secondV = v1.subtract(delta_v);
            double error = -Double.MAX_VALUE;
            for (int i = 0; i < n - 1; i++) {
                double diff = Math.abs(secondV.getEntry(i, 0).abs() - firstV.getEntry(i, 0).abs());
                if (diff > error) {
                    error = diff;
                }
            }

            if (Math.abs(error) < 0.0001 || iter >= maxIter) {
                rep = 0;
            }
            firstV = secondV;
            //secondV = null;
            iter ++;
        } while (rep == 1);
        double[] Vm = new double[n];
        Vm[0] = 1;

        for (int i = 0; i < n - 1; i++) {
            Vm[i + 1] = secondV.getEntry(i, 0).abs();
        }

        int numberOfPosViolations = 0;
        int numberOfNegViolations = 0;
        for (int i = 0; i < Vm.length; i++) {
            // a violation occurs if the value lies outside of the interval <0.95, 1.05>
            if (Vm[i] < 0.95) {

                numberOfNegViolations++;
            } else if (Vm[i] > 1.05) {

                numberOfPosViolations++;
            }

            // add the difference from perfect voltage
            result.deltaV += Math.abs(1.0 - Vm[i]);
        }
        result.numberOfPosViolations += numberOfPosViolations;
        result.numberOfNegViolations += numberOfNegViolations;

        result.numberOfPosViolationsVector[slot] = numberOfPosViolations;
        result.numberOfNegViolationsVector[slot] = numberOfNegViolations;

        result.addVoltages(slot, Vm);
    }

    private static FieldMatrix<Complex> createComplexMatrix(Complex complex, int i, int j) {
        Complex[][] data = new Complex[i][j];
        for (int k = 0; k < i; k++) {
            for (int l = 0; l < j; l++) {
                data[k][l] = complex;
            }
        }
        FieldMatrix<Complex> ret = MatrixUtils.createFieldMatrix(data);
        return ret;
    }

    private static RealVector createVector(int initValue, int size) {
        double[] vData = new double[size];
        for (int i = 0; i < size; i++) {
            vData[i] = initValue;
        }
        return MatrixUtils.createRealVector(vData);
    }

    private static double getQ(double P)
    {
        double powerFactor = ConfigurationProvider.getInstance().getDoubleProperty("renewableScenario.powerFactor");
        double S = P / powerFactor;
        double Q = Math.sqrt(Math.pow(S, 2) - Math.pow(P, 2));
        return Q;
    }

}
