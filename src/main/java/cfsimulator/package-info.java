/**
 * <pre>
 * CFSimulator is a project created by PAVEL JANOVSKY to support his PhD research.
 * This project implements a multi-agent based simulation framework for coalition formation.
 *
 * PLEASE CITE THE FOLLOWING PUBLICATION:
 * Multi-Agent Simulation Framework for Large-Scale Coalition Formation,
 * P. Janovsky, S. A. DeLoach, IEEE/WIC/ACM International Conference on Web Intelligence, 2016.
 *
 * PUBLICATIONS BASED ON THIS CODE ARE SHOWN HERE:
 * https://www.researchgate.net/profile/Pavel_Janovsky2
 *
 * AUTHOR CONTACT INFORMATION:
 * janovsky.paja@gmail.com
 *
 * The program entry point is cfsimulator.simulation.creator.Creator.
 *
 * Javadoc documentation is available in the folder documentation.
 * </pre>
 */
package cfsimulator;