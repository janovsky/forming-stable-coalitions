package cfsimulator.utils;

import java.util.Random;

/**
 * RandomProvider provides the random number generator.
 * For the case of experiments: ONLY this Provider should be used to generate random numbers.
 * Seed is loaded from configuration, seed == 0 denotes random seed
 *
 * Created by janovsky on 2/16/15.
 */
public class RandomProvider {
    private static RandomProvider instance;
    private Random random;
    private Random temporaryRandom;
    private long seed;
    private boolean useTemporary = false;
    // to be used ONLY for picking subsets of agents to update in AgentStorage
    private Random agentStorageRandom;

    protected RandomProvider() {
    }

    public static RandomProvider getInstance() {
        if (instance == null) {
            instance = new RandomProvider();
        }
        return instance;
    }

    public void init(long seed) {
        this.seed = seed;
        if (seed == 0) {
            this.random = new Random();
            this.agentStorageRandom = new Random();
        } else {
            this.random = new Random(seed);
            this.agentStorageRandom = new Random(seed);
        }
    }

    public Random getRandom() {
        if (useTemporary) {
            return temporaryRandom;
        } else {
            return random;
        }
    }

    public void switchToTemporary() {
        useTemporary = true;
        if (seed == 0) {
            this.temporaryRandom = new Random();
        } else {
            this.temporaryRandom = new Random(seed);
        }
    }

    public void switchToDefault() {
        useTemporary = false;
    }

    /**
     * to be used ONLY for picking subsets of agents to update in AgentStorage
     *
     * @return
     */
    public Random getAgentStorageRandom() {
        return agentStorageRandom;
    }

}
