package cfsimulator.utils;

import java.io.*;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * EvaluationProvider is used to parse the generated precomputed evaluations of all possible coalitions.
 *
 * Created by janovsky on 3/27/15.
 */
public class EvaluationFunctionParser {

    public static HashMap<String, Integer> parseEvaluationFunction(File file) {
        HashMap<String, Integer> ret = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                processLine(line, ret);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    private static void processLine(String line, HashMap<String, Integer> ret) {
        StringTokenizer st = new StringTokenizer(line);
        int value = Integer.parseInt(st.nextToken());
        st.nextToken();
        String coalitionString = st.nextToken();
        ret.put(coalitionString, value);
    }
}
