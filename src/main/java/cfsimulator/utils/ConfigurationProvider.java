package cfsimulator.utils;


import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * ConfigurationProvider reads key-value pairs from
 * the configuration file (configuration.properties)
 * OR the program arguments.
 * NOTE that the program argument configuration has higher priority and can therefore overwrite
 * the configuration file input
 *
 * Created by janovsky on 2/16/15.
 */
public class ConfigurationProvider {

    private static ConfigurationProvider instance;
    private Configuration config;
    private HashMap<String, String> programArguments = new HashMap<>();

    protected ConfigurationProvider() {
    }

    public static ConfigurationProvider getInstance() {
        if (instance == null) {
            instance = new ConfigurationProvider();
        }
        return instance;
    }

    public void init(String configFileName, String[] args) {
        try {
            config = new PropertiesConfiguration(configFileName);
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
        if (config.getBoolean("configuration.programArguments")) {
            parseArguments(args);
        }
    }

    private void parseArguments(String[] args) {
        assert args.length % 2 == 0;
        for (int i = 0; i < args.length; i += 2) {
            String type = args[i];
            assert type.charAt(0) == '-';
            type = type.substring(1);
            String value = args[i + 1];
            programArguments.put(type, value);
        }
    }

    public String getStringProperty(String key) {
        String ret = programArguments.get(key);
        if (ret != null) return ret;
        return config.getString(key);
    }

    public int getIntProperty(String key) {
        String ret = programArguments.get(key);
        if (ret != null) return Integer.parseInt(ret);
        return config.getInt(key);
    }

    public Double getDoubleProperty(String key) {
        String ret = programArguments.get(key);
        if (ret != null) return Double.parseDouble(ret);
        return config.getDouble(key);
    }

    public boolean getBooleanProperty(String key) {
        String ret = programArguments.get(key);
        if (ret != null) return Boolean.parseBoolean(ret);
        return config.getBoolean(key);
    }

    public ArrayList<String> getListProperty(String key) {
        String retString = programArguments.get(key);
        ArrayList<String> ret = new ArrayList<>();
        if (retString == null) {
            retString = config.getString(key);
        }
        StringTokenizer st = new StringTokenizer(retString, ";");
        while (st.hasMoreTokens()) {
            ret.add(st.nextToken());
        }
        return ret;
    }

    public void setValue(String key, String value) {
        programArguments.put(key, value);
    }
}
