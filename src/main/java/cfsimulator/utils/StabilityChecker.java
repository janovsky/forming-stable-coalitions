package cfsimulator.utils;

import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;
import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionFactory;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.simulation.simulator.AgentStorage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * StabilityChecker determines the ratio of stable coalitions within the given coalition structure
 * by using breath first search to find dominating sub-coalitions.
 *
 * Created by janovsky on 10/22/15.
 */
public class StabilityChecker {
    private final boolean VERBOSE = ConfigurationProvider.getInstance().getBooleanProperty("simulation.stabilityChecker.verbose");
    private final int maxDeviateCoalitionSize;
    private CoalitionStructure coalitionStructure;
    private AgentStorage agentStorage;
    private Logger logger = Logger.getLogger(this.getClass());
    private int counter = 0;
    private boolean sizeLimitReached;
    private StopWatch timer = new StopWatch();


    public StabilityChecker(CoalitionStructure coalitionStructure, AgentStorage agentStorage) {
        this.coalitionStructure = coalitionStructure;
        this.agentStorage = agentStorage;
        maxDeviateCoalitionSize = ConfigurationProvider.getInstance().getIntProperty("simulation.stabilityChecker.maxDeviateCoalitionSize");
    }

    public static Domination getDominationStatus(Coalition c, Coalition originalCoalition, boolean fullCheck) {
        if (fullCheck) {
            Domination dominating = Domination.EQUAL;
            for (String agent : c.getAgentsOrdered()) {
                Domination d = getDominationSingleAgent(agent, c, originalCoalition);
                if (d.equals(Domination.DOMINATED)) return d;
                else if (d.equals(Domination.DOMINATING)) dominating = d;
                    //if all agents must profit from deviation, equal deviation is not sufficient, therefore return dominated
                else if (d.equals(Domination.EQUAL) && ConfigurationProvider.getInstance().getBooleanProperty("simulation.stability.allAgentsMustProfit")) {
                    return Domination.DOMINATED;
                }
            }
            return dominating;
        } else {
            //only check last agent, the rest were checked in previous steps of BFS
            String a = c.getAgentsOrdered().get(c.getAgentsOrdered().size() - 1);
            return getDominationSingleAgent(a, c, originalCoalition);
        }
    }

    private static Domination getDominationSingleAgent(String a, Coalition c, Coalition originalCoalition) {
        double newContribution = 0;
        double originalContribution = 0;

        if (ConfigurationProvider.getInstance().getStringProperty("profitSharing").equals(Coalition.ProfitSharing.EntryMarginalContribution.toString())) {
            newContribution = c.getProfit(a);
            originalContribution = originalCoalition.getProfit(a);
        }
        if (ConfigurationProvider.getInstance().getStringProperty("profitSharing").equals(Coalition.ProfitSharing.CurrentMarginalContribution.toString())) {
            newContribution = c.getFairProfit(a);
            originalContribution = originalCoalition.getFairProfit(a);
        }
        if (newContribution < originalContribution) {
            return Domination.DOMINATED;
        } else if (newContribution > originalContribution) {
            return Domination.DOMINATING;
        } else return Domination.EQUAL;
    }

    public static Result alphaStable(int alpha, CoalitionStructure coalitionStructure, HashSet<String> toSkip, AgentStorage agentStorage) {
        Iterator<Coalition> coalitionIterator = coalitionStructure.getOpenCoalitionIterator();
        int stableCount = 0;
        boolean unresolved = false;
        while (coalitionIterator.hasNext()) {
            Coalition coalition = coalitionIterator.next();
            // we already know this coalition is unstable, therefore skip it and do not increment the counter
            if (toSkip.contains(coalition.getName())) continue;
            Combinations comb = new Combinations(coalition, agentStorage);

            //otherwise there is no point in testing
            if (coalition.getSize() >= alpha) {
                if (!comb.combination(coalition.getAgentsOrdered(), alpha)) {
                    toSkip.add(coalition.getName());
//                    if(VERBOSE) logger.info(coalition.toEntryOrderedString() + " is NOT " + n + "-stable");
                    continue;
                } else {
                    unresolved = true;
                }
            }
            // we tested all permutations of all combinations, this coalition is stable
            stableCount++;
//            if(VERBOSE) logger.info(coalition.toEntryOrderedString() + " is " + n + "-stable");
        }
        return new Result(stableCount, unresolved);
    }

    public double[] getStabilityRatio(int maxAlpha) {
        double[] ret = new double[]{0, 0};

        ret[0] = stabilityCheck(maxAlpha);
        ret[1] = 0;

        if (VERBOSE) logger.info("strong stability: " + ret[0] + " weak stability: " + ret[1]);


        return ret;
    }

    private boolean isStable(Coalition coalition) {
        //create a new coalition, this will recalculate the marginal contributions which might not have been up to date
        Coalition originalCoalition = CoalitionFactory.getInstance().createCoalition(coalition.getAgentsOrdered(), agentStorage);

        if (ConfigurationProvider.getInstance().getStringProperty("profitSharing").equals(Coalition.ProfitSharing.CurrentMarginalContribution.toString())) {
            return enumerateAllSubsets(originalCoalition.getAgentsOrdered(), originalCoalition);
        }
        if (ConfigurationProvider.getInstance().getStringProperty("profitSharing").equals(Coalition.ProfitSharing.EntryMarginalContribution.toString())) {
            Iterator<Agent> agentsIterator = originalCoalition.getAgentsIterator();
            boolean stable;
            sizeLimitReached = false;

            while (agentsIterator.hasNext()) {
                stable = orderedBreadthFirstSearch(agentsIterator.next(), originalCoalition);
                if (!stable) return false;
            }
            return true;
        }
        //default, unreachable statement
        return false;
    }

    /**
     * breadth first search through all possible permutations of all sizes
     * Note: no need for close list
     *
     * @param root
     * @param originalCoalition
     * @return
     */
    private boolean orderedBreadthFirstSearch(Agent root, Coalition originalCoalition) {
        LinkedList<Coalition> openList = new LinkedList<>();
        openList.add(CoalitionFactory.getInstance().createCoalition(root, agentStorage));
        while (!openList.isEmpty()) {
            Coalition c = openList.poll();
            System.out.println("CHECKING: " + c.toEntryOrderedString());
            Domination d = getDominationStatus(c, originalCoalition, false);
            //if dominating then the originalCoalition is not stable, if dominated then do not continue with coalition c
            if (d.equals(Domination.DOMINATING)) return false;
            else if (d.equals(Domination.EQUAL)) {
                openList.addAll(expand(c, originalCoalition));
            }
        }
        //we didn't find a dominating sub-coalition starting from root agent
        return true;
    }

    /**
     * expand coalition node by adding a single agent to each new node
     *
     * @param c
     * @param originalCoalition
     * @return
     */
    private ArrayList<Coalition> expand(Coalition c, Coalition originalCoalition) {
        Iterator<Agent> agentsIterator = originalCoalition.getAgentsIterator();
        ArrayList<Coalition> newCoalitions = new ArrayList<>();
        while (agentsIterator.hasNext()) {
            Agent a = agentsIterator.next();
            if (!c.contains(a.getName())
                    && (maxDeviateCoalitionSize <= 0 || c.getSize() < maxDeviateCoalitionSize)) {
                ArrayList<String> names = new ArrayList<>(c.getAgentsOrdered());
                names.add(a.getName());
                newCoalitions.add(CoalitionFactory.getInstance().createCoalition(names, agentStorage));
            } else if (c.getSize() == maxDeviateCoalitionSize) {
                sizeLimitReached = true;
            }
        }
        return newCoalitions;
    }

    /**
     * source: http://stackoverflow.com/questions/22280078/how-to-write-iterative-algorithm-for-generate-all-subsets-of-a-set
     *
     * @param agents
     */
    public boolean enumerateAllSubsets(ArrayList<String> agents, Coalition originalCoalition) {
        byte[] counter = new byte[agents.size()];

        while (true) {
            ArrayList<String> newNames = new ArrayList<>();
            // Print combination
            for (int i = 0; i < counter.length; i++) {
                if (counter[i] != 0)
                    newNames.add(agents.get(i));
            }
            if (!newNames.isEmpty()) {
                Domination d = getDominationStatus(CoalitionFactory.getInstance().createCoalition(newNames, agentStorage), originalCoalition, true);
                if (d.equals(Domination.DOMINATING)) {
                    return false;
                }
            }
            // Increment counter
            int i = 0;
            while (i < counter.length && counter[i] == 1)
                counter[i++] = 0;
            if (i == counter.length)
                break;
            counter[i] = 1;
        }
        return true;
    }

    public double stabilityCheck(int maxAlpha) {
        if (maxAlpha <= 0) maxAlpha = Integer.MAX_VALUE;
        int coalCount = coalitionStructure.getOpenCoalitionMap().size();
        ArrayList<Integer> stabnList = new ArrayList<>();
        ArrayList<Long> computationTimeList = new ArrayList<>();
        HashSet<String> toSkip = new HashSet<>();
        stabnList.add(coalCount);
        computationTimeList.add((long) 0);
        int maxCoalitionSize = 0;
        Iterator<Coalition> openCoalitionIterator = coalitionStructure.getOpenCoalitionIterator();
        while (openCoalitionIterator.hasNext()) {
            int size = openCoalitionIterator.next().getSize();
            if (size > maxCoalitionSize) maxCoalitionSize = size;
        }
        timer.start();
        for (int n = 1; n <= Math.min(maxCoalitionSize, maxAlpha); n++) {
            Result stabn = alphaStable(n, coalitionStructure, toSkip, agentStorage);
            computationTimeList.add(timer.getTime());
            stabnList.add(stabn.result);
            if (VERBOSE) logger.info(n + "-stability calculated");
            if (!stabn.unresolved) break;
        }
        if (VERBOSE) logger.info("Tested CS: " + coalitionStructure.toOrderedString());
        if (ConfigurationProvider.getInstance().getBooleanProperty("simulation.stability.generateStabilityAlphaData")) {
            generateStabilityVsAlphaData(stabnList, coalCount, computationTimeList);
        }
        return ((double) stabnList.get(stabnList.size() - 1)) / coalCount;
    }


    private void generateStabilityVsAlphaData(ArrayList<Integer> stableCounts, int coalCount, ArrayList<Long> computationTimeList) {
        StringBuilder sb = new StringBuilder();
        sb.append("Alpha, CSStability, RunTime \n");
        double ratio;
        for (int alpha = 0; alpha < stableCounts.size(); alpha++) {
            ratio = (double) stableCounts.get(alpha) / coalCount;
            sb.append(alpha).append(", ").append(ratio).append(", ").append(computationTimeList.get(alpha)).append("\n");
        }

        String ef = ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction");
        ef = ef.substring(ef.lastIndexOf(".") + 1);
        String s = agentStorage.getAgent("Agent1").getStrategy().toString();
        String fileName = "process-experiments/stabilityAlpha/stabilityAlpha-" + ef + "-" + s + "-" + agentStorage.getNumberOfAgents() + ".txt";

        File file = new File(fileName);
        file.getParentFile().mkdirs();

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(file, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.print(sb);
        writer.close();
    }

    public enum Domination {
        DOMINATING, DOMINATED, EQUAL
    }

    public static class Result {
        public final int result;
        public final boolean unresolved;

        private Result(int result, boolean unresolved) {
            this.result = result;
            this.unresolved = unresolved;
        }
    }

}
