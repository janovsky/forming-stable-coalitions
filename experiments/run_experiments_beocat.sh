#!/bin/bash
instanceset=$1
cpus="24"
datain="data.in"
dataout="data.out"
dataouthead="data.out.head"
head="head.in"
mem="2"

#rm -r dpidpinput

#echo "Pre-computing evaluations"
#./parallel_experiments.sh -j precomputer-artifact/CFSimulator.jar -c $datain -o "precomputer.out" -m $mem"g" -v -s $cpus/:
#java -jar precomputer-artifact/CFSimulator.jar $datain

echo "Jobs to run: "
wc -l $datain

echo "Running experiments"
./parallel_experiments.sh -j artifact/CFSimulator.jar -c $datain -o $dataout -m $mem"g" -v -s $cpus/:

./addhead.sh $head $dataout $dataouthead

#echo "Generating optimal solutions"

#./generateOptimalSolutions.sh