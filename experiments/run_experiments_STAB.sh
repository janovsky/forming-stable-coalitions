#!/bin/bash
instanceset=$1
cpus="19"
datain="dataAllMustProfit.in"
dataout="dataAllMustProfit.out"
dataouthead="dataAllMustProfit.out.head"
head="head.in"
mem="2"

echo "Jobs to run: "
wc -l $datain

# Send email notification
python sendemail.py -m "Starting experiments"

echo "Running experiments"
./parallel_experiments.sh -j artifact/CFSimulator.jar -c $datain -o $dataout -m $mem"g" -v -s $cpus/:

./addhead.sh $head $dataout $dataouthead

# Send email notification
python sendemail.py -m "Experiment 1 completed"



datain="dataEqual.in"
dataout="dataEqual.out"
dataouthead="dataEqual.out.head"

echo "Jobs to run: "
wc -l $datain

echo "Running experiments"
./parallel_experiments.sh -j artifact/CFSimulator.jar -c $datain -o $dataout -m $mem"g" -v -s $cpus/:

./addhead.sh $head $dataout $dataouthead

# Send email notification
python sendemail.py -m "Experiment 2 completed"

datain="dataEqualAllMustProfit.in"
dataout="dataEqualAllMustProfit.out"
dataouthead="dataEqualAllMustProfit.out.head"

echo "Jobs to run: "
wc -l $datain

echo "Running experiments"
./parallel_experiments.sh -j artifact/CFSimulator.jar -c $datain -o $dataout -m $mem"g" -v -s $cpus/:

./addhead.sh $head $dataout $dataouthead

# Send email notification
python sendemail.py -m "Experiment 3 completed"

# Send email notification
python sendemail.py -m "ALL experiments completed"