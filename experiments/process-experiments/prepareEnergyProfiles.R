data <- read.csv(file="LD_2014_Jan.csv", head=TRUE, sep=",")
colnames(data)[2] <- "time"
averages <- ddply(data, .(time), numcolwise(mean))
write.table(averages, "LD_2014_Jan_AVG.txt", sep=";", col.names = F, row.names = F)
