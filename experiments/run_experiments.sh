#!/bin/bash
instanceset=$1
cpus="24"
datain="dataStabIter.in"
dataout="dataStabIter.out"
dataouthead="dataStabIter.out.head"
head="head.in"
mem="2"

# Send email notification
python sendemail.py -m "Stability by iteration count: Experiments started"

echo "Jobs to run: "
wc -l $datain

echo "Running experiments"
./parallel_experiments.sh -j artifact/CFSimulator.jar -c $datain -o $dataout -m $mem"g" -v -s $cpus/:

./addhead.sh $head $dataout $dataouthead


# Send email notification
python sendemail.py -m "Stability by iteration count: ALL experiments completed"