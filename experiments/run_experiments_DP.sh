#!/bin/bash
instanceset=$1
cpus="12"
datain="dataSmallResources.in"
dataout="dataSmallResources.out"
dataouthead="dataSmallResources.out.head"
head="head.in"
mem="2"

# Send email notification
python sendemail.py -m "DP Experiments started"


echo "Pre-computing evaluations"
#./parallel_experiments.sh -j precomputer-artifact/CFSimulator.jar -c $datain -o "precomputer.out" -m $mem"g" -v -s $cpus/:
rm -r dpidpinput
java -jar precomputer-artifact/CFSimulator.jar $datain

echo "Jobs to run: "
wc -l $datain

echo "Running experiments"
./parallel_experiments.sh -j artifact/CFSimulator.jar -c $datain -o $dataout -m $mem"g" -v -s $cpus/:

./addhead.sh $head $dataout $dataouthead

echo "Generating optimal solutions"

./generateOptimalSolutions.sh

# Send email notification
python sendemail.py -m "DP Experiments finished"